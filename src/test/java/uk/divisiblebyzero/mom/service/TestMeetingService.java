package uk.divisiblebyzero.mom.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.divisiblebyzero.mom.MomApplication;
import uk.divisiblebyzero.mom.domain.Meeting;
import uk.divisiblebyzero.mom.domain.MeetingEntry;
import uk.divisiblebyzero.mom.domain.MeetingEntryType;
import uk.divisiblebyzero.mom.domain.MeetingItem;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by: Matthew Smalley
 * Date: 06/09/2015
 */
@ActiveProfiles({"tests"})
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(MomApplication.class)
public class TestMeetingService {
    private String meetingId;

    @Resource
    private MeetingService meetingService;

    @Resource
    private MeetingRepository meetingRepository;

    private Meeting createMeeting(String meetingSeries) {
        Meeting meeting = new Meeting();
        meeting.setMeetingSeries(meetingSeries);
        meeting.setMeetingDate(LocalDate.now());

        List<MeetingEntry> entries = Collections.singletonList(new MeetingEntry("agree what is in the next release", MeetingEntryType.AGENDA));
        MeetingItem meetingItem = new MeetingItem("next release", entries);

        meeting.setMeetingItems(Collections.singletonList(meetingItem));

        meeting.setAttendees(Arrays.asList(new String[]{"Tom", "Dick", "Harry"}));

        return meeting;
    }

    @Test
    public void testSaveMeeting() {
        Meeting meeting = createMeeting("Party monthly update");

        meetingRepository.save(meeting);

        meetingId = meeting.getId();

        meeting = meetingRepository.findOne(meetingId);
        meeting.getMeetingItems().get(0).getMeetingEntries().addAll(Collections.singletonList(new MeetingEntry("agreed what is in the next release", MeetingEntryType.MINUTE)));

        meetingRepository.save(meeting);
    }

    @Test
    public void testGetMeetingByMeetingSeries() {
        String meetingSeries1 = UUID.randomUUID().toString();
        String meetingSeries2 = UUID.randomUUID().toString();

        meetingRepository.save(createMeeting(meetingSeries1));
        meetingRepository.save(createMeeting(meetingSeries1));
        meetingRepository.save(createMeeting(meetingSeries2));

        List<Meeting> meetings = meetingRepository.findByMeetingSeries(meetingSeries1);
        assertEquals(2, meetings.size());
    }

    @Test
    public void testGetCategories() {
        Meeting meeting = createMeeting(UUID.randomUUID().toString());
        meetingRepository.save(meeting);
        List<String> categories = meetingService.getMeetingItemCategories();
        for (MeetingItem meetingItem : meeting.getMeetingItems()) {
            assertTrue(categories.contains(meetingItem.getCategory()));
        }

        Set<String> categorySet = new HashSet<>();
        categorySet.addAll(categories);
        assertEquals(categories.size(), categorySet.size());
    }
}
