package uk.divisiblebyzero.mom.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.divisiblebyzero.mom.MomApplication;
import uk.divisiblebyzero.mom.domain.Action;
import uk.divisiblebyzero.mom.domain.LogEntry;

import javax.annotation.Resource;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

/**
 * Created by: Matthew Smalley
 * Date: 01/09/2015
 */
@ActiveProfiles({"tests"})
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(MomApplication.class)
public class TestActionService {
    @Resource
    ActionRepository actionRepository;

    @Test
    public void testSave() {
        String testCategory = "TestCategory";
        int numberOfActions = actionRepository.findByCategory(testCategory).size();
        Action action = new Action();
        action.setAssignee("Matthew");
        action.setCategory(testCategory);
        action.setCreated(LocalDate.now());
        action.setDue(LocalDate.now().plusDays(7));
        action.setSummary("Do this thing in a week");

        actionRepository.save(action);

        assertEquals(numberOfActions + 1, actionRepository.findByCategory(testCategory).size());

        LogEntry logEntry = new LogEntry();
        logEntry.setEntryDate(LocalDate.now());
        logEntry.setText("Created action");

        actionRepository.save(action);

        assertEquals(numberOfActions + 1, actionRepository.findByCategory(testCategory).size());
    }
}
