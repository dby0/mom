package uk.divisiblebyzero.mom.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import uk.divisiblebyzero.mom.domain.Meeting;
import uk.divisiblebyzero.mom.domain.MeetingSeries;

/**
 * Created by: Matthew Smalley
 * Date: 01/09/2015
 */
@Configuration
public class SpringConfig {
    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {

        return new RepositoryRestConfigurerAdapter() {
            @Override
            public void configureRepositoryRestConfiguration(
                    RepositoryRestConfiguration config) {
                config.exposeIdsFor(Meeting.class, MeetingSeries.class);
            }
        };
    }
}
