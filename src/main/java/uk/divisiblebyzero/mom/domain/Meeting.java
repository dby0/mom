package uk.divisiblebyzero.mom.domain;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 06/09/2015
 */
public class Meeting {
    @Id
    private String id;
    private String meetingSeries;
    private List<MeetingItem> meetingItems;
    private List<String> attendees;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate meetingDate;

    public LocalDate getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(LocalDate meetingDate) {
        this.meetingDate = meetingDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<MeetingItem> getMeetingItems() {
        return meetingItems;
    }

    public void setMeetingItems(List<MeetingItem> meetingItems) {
        this.meetingItems = meetingItems;
    }

    public List<String> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<String> attendees) {
        this.attendees = attendees;
    }

    public String getMeetingSeries() {
        return meetingSeries;
    }

    public void setMeetingSeries(String meetingSeries) {
        this.meetingSeries = meetingSeries;
    }
}
