package uk.divisiblebyzero.mom.domain;

import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 07/02/2016
 */
public class MeetingSeries {
    private String id;
    private String name;
    private List<String> standingTopics;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getStandingTopics() {
        return standingTopics;
    }

    public void setStandingTopics(List<String> standingTopics) {
        this.standingTopics = standingTopics;
    }
}
