package uk.divisiblebyzero.mom.domain;

import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 10/09/2015
 */
public class MeetingItem {
    private String category;
    private List<MeetingEntry> meetingEntries;

    public MeetingItem() {

    }

    public MeetingItem(String category, List<MeetingEntry> meetingEntries) {
        this.category = category;
        this.meetingEntries = meetingEntries;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<MeetingEntry> getMeetingEntries() {
        return meetingEntries;
    }

    public void setMeetingEntries(List<MeetingEntry> meetingEntries) {
        this.meetingEntries = meetingEntries;
    }
}
