package uk.divisiblebyzero.mom.domain;

import java.time.LocalDate;

/**
 * Created by: Matthew Smalley
 * Date: 01/09/2015
 */
public class LogEntry {
    private String text;
    private LocalDate entryDate;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDate getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDate entryDate) {
        this.entryDate = entryDate;
    }
}
