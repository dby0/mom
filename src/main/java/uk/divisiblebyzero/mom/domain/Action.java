package uk.divisiblebyzero.mom.domain;

import org.bson.types.ObjectId;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 01/09/2015
 */
public class Action {
    private ObjectId id;
    private String assignee;
    private String summary;
    private String category;
    private LocalDate created;
    private LocalDate due;
    private List<LogEntry> logs;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public LocalDate getDue() {
        return due;
    }

    public void setDue(LocalDate due) {
        this.due = due;
    }

    public List<LogEntry> getLogs() {
        return logs;
    }

    public void setLogs(List<LogEntry> logs) {
        this.logs = logs;
    }
}
