package uk.divisiblebyzero.mom.domain;

import org.springframework.data.annotation.Id;

/**
 * Created by: Matthew Smalley
 * Date: 13/09/2015
 */
public class Person {
    @Id
    private String name;
    private String shortName;
    private String initials;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
