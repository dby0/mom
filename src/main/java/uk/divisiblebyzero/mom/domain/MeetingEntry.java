package uk.divisiblebyzero.mom.domain;

/**
 * Created by: Matthew Smalley
 * Date: 06/09/2015
 */
public class MeetingEntry {
    private String description;
    private MeetingEntryType meetingEntryType;
    private String contents;

    public MeetingEntry() {
    }

    public MeetingEntry(String description, MeetingEntryType meetingEntryType) {
        this.description = description;
        this.meetingEntryType = meetingEntryType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MeetingEntryType getMeetingEntryType() {
        return meetingEntryType;
    }

    public void setMeetingEntryType(MeetingEntryType meetingEntryType) {
        this.meetingEntryType = meetingEntryType;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}
