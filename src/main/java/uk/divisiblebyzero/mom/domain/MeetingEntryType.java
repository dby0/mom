package uk.divisiblebyzero.mom.domain;

/**
 * Created by: Matthew Smalley
 * Date: 10/09/2015
 */
public enum MeetingEntryType {
    AGENDA, MINUTE, ACTION
}
