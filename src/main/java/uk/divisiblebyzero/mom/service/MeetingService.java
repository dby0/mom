package uk.divisiblebyzero.mom.service;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import uk.divisiblebyzero.mom.domain.Meeting;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 06/09/2015
 */
@Service
public class MeetingService {

    @Resource
    private MongoTemplate mongoTemplate;

    public List<String> getMeetingSeries() {
        return mongoTemplate.getCollection(mongoTemplate.getCollectionName(Meeting.class)).distinct("meetingSeries");
    }

    public List<String> getMeetingItemCategories() {
        return mongoTemplate.getCollection(mongoTemplate.getCollectionName(Meeting.class)).distinct("meetingItems.category");
    }
}
