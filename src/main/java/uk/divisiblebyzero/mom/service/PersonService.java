package uk.divisiblebyzero.mom.service;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import uk.divisiblebyzero.mom.domain.Person;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 13/09/2015
 */
@Service
public class PersonService {
    @Resource
    private MongoTemplate mongoTemplate;

    public void savePerson(Person person) {
        mongoTemplate.save(person);
    }

    public Person getPersxonByName(String name) {
        Person person = mongoTemplate.findById(name, Person.class);
        if (person == null) {
            person = new Person();
            person.setName(name);
            mongoTemplate.save(person);
        }
        return person;
    }

    public List<Person> gextAllPersons() {
        return mongoTemplate.findAll(Person.class);
    }
}
