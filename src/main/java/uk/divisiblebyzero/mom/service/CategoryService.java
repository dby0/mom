package uk.divisiblebyzero.mom.service;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import uk.divisiblebyzero.mom.domain.Action;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 01/09/2015
 */
@Service
public class CategoryService {
    @Resource
    private MongoTemplate mongoTemplate;

    public List<String> getCategories() {
        return mongoTemplate.getCollection(mongoTemplate.getCollectionName(Action.class)).distinct("category");
    }
}
