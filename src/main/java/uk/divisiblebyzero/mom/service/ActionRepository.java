package uk.divisiblebyzero.mom.service;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uk.divisiblebyzero.mom.domain.Action;

import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 31/01/2016
 */
@RepositoryRestResource
public interface ActionRepository extends MongoRepository<Action, ObjectId> {
    List<Action> findByCategory(String category);
}
