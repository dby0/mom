package uk.divisiblebyzero.mom.service;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uk.divisiblebyzero.mom.domain.Person;

/**
 * Created by: Matthew Smalley
 * Date: 31/01/2016
 */
@RepositoryRestResource
public interface PersonRepository extends MongoRepository<Person, String> {
}
