package uk.divisiblebyzero.mom.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import uk.divisiblebyzero.mom.domain.Action;
import uk.divisiblebyzero.mom.service.ActionRepository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 01/09/2015
 */
@Controller
@RequestMapping("/actions")
public class ActionController {

    @Resource
    private ActionRepository actionRepository;

    @RequestMapping(value = "{category}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Action> getActions(@PathVariable String category) {
        return actionRepository.findByCategory(category);
    }
}
