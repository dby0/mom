package uk.divisiblebyzero.mom.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import uk.divisiblebyzero.mom.domain.Meeting;
import uk.divisiblebyzero.mom.service.MeetingRepository;
import uk.divisiblebyzero.mom.service.MeetingService;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 08/09/2015
 */
@Controller
@RequestMapping("/api/meetings")
public class MeetingController {
    @Resource
    private MeetingService meetingService;

    @Resource
    private MeetingRepository meetingRepository;

    @RequestMapping(value = "/series/{meetingSeries}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Meeting> getMeetingsByMeetingSeries(@PathVariable String meetingSeries) {
        return meetingRepository.findByMeetingSeries(meetingSeries);
    }

    @RequestMapping(value = "/series", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> getMeetingSeries() {
        return meetingService.getMeetingSeries();
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> getMeetingItemCategories() {
        return meetingService.getMeetingItemCategories();
    }

    @RequestMapping(value = "/savemeeting", method = RequestMethod.POST)
    public
    @ResponseBody
    Meeting saveMeeting(@RequestBody Meeting meeting) {
        meetingRepository.save(meeting);
        return meeting;
    }

    @RequestMapping(value = "/deletemeeting", method = RequestMethod.POST)
    public
    @ResponseBody
    void deleteMeeting(@RequestBody Meeting meeting) {
        meetingRepository.delete(meeting);
    }
}
