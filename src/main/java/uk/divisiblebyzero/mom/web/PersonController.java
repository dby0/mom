package uk.divisiblebyzero.mom.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import uk.divisiblebyzero.mom.domain.Person;
import uk.divisiblebyzero.mom.service.PersonRepository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 13/09/2015
 */
@Controller
@RequestMapping("/api/person")
public class PersonController {

    @Resource
    private PersonRepository personRepository;

    @RequestMapping("/{name}")
    public
    @ResponseBody
    Person getPerson(@PathVariable String name) {
        return personRepository.findOne(name);
    }

    @RequestMapping("/")
    public
    @ResponseBody
    List<Person> getAllPersons() {
        return personRepository.findAll();
    }
}
