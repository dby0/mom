package uk.divisiblebyzero.mom.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import uk.divisiblebyzero.mom.service.CategoryService;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 01/09/2015
 */
@Controller
@RequestMapping("/categories")
public class CategoryController {
    @Resource
    private CategoryService categoryService;

    @RequestMapping("/")
    public
    @ResponseBody
    List<String> getCategories() {
        return categoryService.getCategories();
    }
}
