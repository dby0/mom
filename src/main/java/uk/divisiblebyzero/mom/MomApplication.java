package uk.divisiblebyzero.mom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by: Matthew Smalley
 * Date: 31/01/2016
 */
@SpringBootApplication
public class MomApplication {


    public static void main(String[] args) throws Exception {
        SpringApplication.run(MomApplication.class, args);
    }

}
