    momControllers.controller("MomMeetingSeriesCtrl", function($scope, $http, $log, $location) {
        $http.get("meetingSeries").then(function(response) {
            $scope.meetingSeries = response.data._embedded.meetingSeries;
        });

        $scope.showMeetings = function (meetingSeries) {
            $location.path("/meetings/" + meetingSeries);
        };

        $scope.addSeries = function() {
            $scope.meetingSeries.push(null);
        };

        $scope.saveSeries = function(series) {
            $http.post("meetingSeries", series).then(function(response) {
                series.id = response.data.id;
                $scope.status = "Saved";
            }, function(response) {
                if (response.status == 403) {
                $scope.status = "Access denied";
            } else {
                $scope.status = "Unknown error saving details";
            }
            });
        };
    });