var momApp = angular.module('momApp', [
  'ngRoute',
  'momControllers',
  'textAngular'
]);



momApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/meetings/:meetingSeries', {
        templateUrl: 'partial.meetings.html',
        controller: 'MomMeetingsCtrl'
      }).
      when('/meetings', {
        templateUrl: 'partial.meetingseries.html',
        controller: 'MomMeetingSeriesCtrl'
      }).
      when('/actions', {
        templateUrl: 'partial.actions.html',
        controller: 'MomActionsCtrl'
      }).
      when('/persons', {
        templateUrl: 'partial.persons.html',
        controller: 'MomPersonsCtrl'
      }).
      otherwise({
        redirectTo: '/meetings'
      });
  }]);