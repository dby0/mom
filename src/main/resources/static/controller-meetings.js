    momControllers.controller("MomMeetingsCtrl", function ($scope, $http, $interval, $filter, $log, $uibModal, $window, $location, $routeParams, $anchorScroll) {
        $scope.meetingSeries = $routeParams.meetingSeries;
        $scope.actions = null;

        $scope.showConfigureMeetingSeries = false;

        $http.get("meetingSeries/search/findOneByName?name=" + $scope.meetingSeries).then(function(response) {
            $scope.series = response.data;
        });

        $scope.saveSeries = function(series) {
            $http.post("meetingSeries", series).then(function(response) {
                series.id = response.data.id;
                $scope.status = "Series Saved";
            }, function(response) {
                if (response.status == 403) {
                $scope.status = "Access denied";
            } else {
                $scope.status = "Unknown error saving details";
            }
            });
        };

        $scope.addAttendee = function(meeting) {
            if (!meeting.attendees) {
                meeting.attendees = [null];
            }
            $scope.conditionallyAddToArray(meeting.attendees);
        };

        $scope.conditionallyAddToArray = function(array) {
            if (!array) {
                array = [null];
            } else {
                if (!(array[array.length-1] == null)) {
                    array.push(null);
                }
            }
        };

        $scope.addTopic = function() {
            if (!$scope.series.standingTopics) {
                $scope.series.standingTopics = [null];
            }
            $scope.conditionallyAddToArray($scope.series.standingTopics);
        };

        $http.get("api/meetings/series")
            .success(function(response) {
                $scope.existingMeetingSeries = response;
            });

        $http.get("api/meetings/categories")
            .success(function(response) {
                $scope.meetingCategories = response;
            });

        $scope.getMeetings = function() {$http.get("api/meetings/series/" + $scope.meetingSeries)
            .success(function(response) {
                $scope.meetings = response;
            });
        }

        $scope.saveMeeting = function(meeting) {
            $http.post("meetings", meeting).then(function(response) {
                meeting.id = response.data.id;
                $scope.status = "Saved";
            }, function(response) {
                if (response.status == 403) {
                $scope.status = "Access denied";
            } else {
                $scope.status = "Unknown error saving details";
            }
            });
        }

        $scope.deleteMeeting = function(meeting, index) {
            if (!meeting.id) {
                $scope.meetings.splice(index,1);
                $scope.status = "Deleted";
            } else {
                $http.delete("meetings/" + meeting.id).then(function(response) {
                    $scope.meetings.splice(index,1);
                    $scope.status = "Deleted";
                }, function(response) {
                    if (response.status == 403) {
                    $scope.status = "Access denied";
                } else {
                    $scope.status = "Unknown error deleting details";
                }
                });
            }
        }

        $scope.revertMeetings = function() {
            $scope.getMeetings();
        };

        $scope.addMeeting = function(newMeetingSeries) {
            var meeting = {meetingSeries: newMeetingSeries, meetingDate: $filter('date')(new Date(), 'yyyy-MM-dd'), meetingItems: []};
            if ($scope.series.standingTopics) {
                for (var i = 0; i < $scope.series.standingTopics.length; i++) {
                    meeting.meetingItems.push({category: $scope.series.standingTopics[i], meetingEntries: []});
                }
            }
            $scope.meetings.push(meeting);
            return meeting;
        };

        $scope.addMeetingItem = function(meeting) {
            if (!meeting.meetingItems) {
                meeting.meetingItems = [];
            }
            meeting.meetingItems.push({category: "New Item", meetingEntries: []});
        };

        $scope.addEntry = function(meetingItem) {
            meetingItem.meetingEntries.push({meetingEntryType: "MINUTE", description: "Enter description", contents: ""});
        };


        $scope.status = "";

        $scope.entryTypes = [
            { value: "AGENDA", text: "Agenda"},
            { value: "ACTION", text: "Action"},
            { value: "MINUTE", text: "Minute"}
        ];

        $scope.getMeetings();


        $scope.selectMeetingEntry = function(entry) {
            $scope.selectedMeetingEntry = entry;
            $scope.showEditMeetingEntryBox = true;

        };

        $scope.deselectMeetingEntry = function() {
            $scope.selectedMeetingEntry = null;
            $scope.showEditMeetingEntryBox = false;
        };



    });